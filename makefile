PREFIX ?= /usr/local
BINDIR ?= $(PREFIX)/bin
MANDIR ?= $(PREFIX)/share/man/man1

.POSIX: install

all: help

help:
	@echo "please run 'make install' as root"
deps:
	cp -R .s4 ~/
install:
	cp ./s4ctl.sh $(BINDIR)/s4ctl
	cp ./s4ctl.1 $(MANDIR)
uninstall:
	rm $(BINDIR)/s4ctl
	rm $(MANDIR)/s4ctl.1

