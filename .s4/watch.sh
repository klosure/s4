#!/bin/sh
#
# template used to create each camera+computer watch file.


# ------ Begin Global vars --------------------------------------
# ID is the camera number (0-n)
ID="XXX"

# We expect /dev/video0 to be the webcam
WEBCAM="/dev/video0"

# Example valid resolutions: 640x480, 1280x720, or 1920x1080
REZ="YYY"

# Your networked storage location
STORAGE="ZZZ"

# Framerate you want to record at
# 30 or 60 depending on what your cameras support
FRAMERATE="WWW"

# Where the local currently recording video is residing.
# When the video is done recording, we copy it over
CACHE_DIR="$HOME/.s4/cache"

# log file location
#S4_LOG_DIR="$HOME/.s4/s4.log"

# FFmpeg arguments:
FF_ARG_STD="-hide_banner -nostats -loglevel fatal"
#FF_ARG_LOG="-loglevel warning"
#FF_ARG_DBG=""
FF_FORMAT="avi"

# Logging disabled by default
# since it is not implemented yet
# ( to enable change to "true" )
#S4_LOG_ENABLED="NO-S4-LOGGING"
# ------ End Global vars ---------------------------------


# ------ Begin functions ---------------------------------
# flip the status files over to reflect 'True'
fun_set_true () {
    echo "false" > "$STORAGE/s4/cameras/$ID/stop.status"
}


# flip the status files over to reflect 'False'
fun_set_false () {
    echo "true" > "$STORAGE/s4/cameras/$ID/stop.status"
}


# Kicks off recording work
# args: 1
# 1: filename to save video as (e.g. 22-2)
fun_record () {
    fun_set_true
    L_VIDNAME=$1
    # -y flag means always overwrite
    # We can safely ignore warning 2086 since we want to word split :)
    # shellcheck disable=SC2086
    ffmpeg -y $FF_ARG_STD -framerate $FRAMERATE -video_size $REZ -i "$WEBCAM" "$CACHE_DIR/$L_VIDNAME.$FF_FORMAT" &
}


# Stop the camera and copy over the completed file
# args: 1
# 1: # file to be copied (f.t.b.c) over to the main storage
fun_stop () {
    L_FTBC=$1
    fun_set_false
    kill $(pidof ffmpeg)
    if ! [ -z "$L_FTBC" ] ; then
	cp $CACHE_DIR/$L_FTBC.$FF_FORMAT $STORAGE/s4/cameras/$ID/$L_FTBC.$FF_FORMAT &
    fi
}


# Checks if our status files have been modified
fun_check () {
    RUNSTAT=$(cat "$STORAGE"/s4/cameras/"$ID"/run.status)
    STOPSTAT=$(cat "$STORAGE"/s4/cameras/"$ID"/stop.status)
    TEN=$(date "+%M")
    TEN_CHECK=$(echo "${TEN%"${TEN#?}"}")
    HOUR_CHECK=$(date +"%H")
    if [ $RUNSTAT = "true" ] && [ $STOPSTAT = "false" ] ; then
	fun_record "$HOUR_CHECK-$TEN_CHECK"
    elif [ $RUNSTAT = "false" ] && [ $STOPSTAT = "true" ] ; then
	fun_stop
    else
	echo "Error State: run & stop status files out of sync.."
    fi
}
# ------ End functions ---------------------------------


# was the script called recursively?
if [ "$#" -eq 1 ] && [ "$1" = "check" ] ; then
    # something must have been changed.. lets find out what.
    fun_check
    exit 0
fi


if [ -d "$STORAGE"/s4 ] ; then
    # where is this script running from?
    THIS_PWD=$(dirname "$0")
    # watch the status file for changes, if it changes, re-run this script..
    LAST_STATUS=$(cat "$STORAGE"/s4/cameras/"$ID"/run.status)

    while true ; do
	sleep 1
	GRAB_STATUS=$(cat $STORAGE/s4/cameras/$ID/run.status)
	if [ "$GRAB_STATUS" != "$LAST_STATUS" ] ; then
	    LAST_STATUS=$GRAB_STATUS
	    $THIS_PWD/watch$ID.sh check & # makes a recursive background call
	fi
    done & # background the loop

    # Lets start keeping track of the time!
    CUR_HOUR=$(date +"%H")
    # get the minutes; e.g. for 13:27, grab 27
    CUR_MIN=$(date "+%M")
    # get the tens position; e.g. for 27, grab 2
    CUR_MIN_CHECK=$(echo "${CUR_MIN%"${CUR_MIN#?}"}")
    while true ; do
	sleep 1
	TEN=$(date "+%M")
	TEN_CHECK=$(echo "${TEN%"${TEN#?}"}")
	HOUR_CHECK=$(date +"%H")
	# has the hour changed?
	if ! [ $HOUR_CHECK = $CUR_HOUR ] ; then
	    fun_stop "$CUR_HOUR-5" # we can always assume the minutes position
	    CUR_HOUR=$HOUR_CHECK
	    CUR_MIN_CHECK=0
	    sleep 5 # make sure we had enough time to stop recording / kick off the copy
	    fun_record "$CUR_HOUR-0"
	# has 10 minutes elapsed?
	elif ! [ $TEN_CHECK = $CUR_MIN_CHECK ] ; then
	    fun_stop "$CUR_HOUR-$CUR_MIN_CHECK"
	    CUR_MIN_CHECK=$TEN_CHECK
	    sleep 5
	    fun_record "$CUR_HOUR-$CUR_MIN_CHECK"
	fi
    done
    
else
    echo "Storage not initialized (or mounted?), quitting.."
    exit 1
fi

exit 0
