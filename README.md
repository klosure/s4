![](logo.png)

[![License](https://is.gd/GbWFMI)](https://opensource.org/licenses/ISC)

Orchestrate and automate handling multiple webcams as security cameras. I had several webcams lying around and wanted a way to easily keep recordings going in a predictable manner. We rely on a common shared storage (NAS) to handle communication between the cameras. In my case I have several networked computers around the house, this allowed me to simply connect up a webcam and I have everything I need.

The idea for this tool is simple. Each computer periodically (every second) checks the NAS to see if it needs to start or stop recording. There is no master controller. As long as you have access to the storage, you have access to control the cameras. One script per camera+computer has to be installed. For instance if you have 4 computers with cameras you want running, each of them will need a watchN.sh script installed.


### Dependencies
- [ffmpeg](https://ffmpeg.org/download.html)
- [ed](https://www.gnu.org/software/ed/manual/ed_manual.html)
- [cron](https://en.wikipedia.org/wiki/Cron)
- POSIX [make](https://pubs.opengroup.org/onlinepubs/009695399/utilities/make.html)
- POSIX [shell](https://pubs.opengroup.org/onlinepubs/009695399/utilities/xcu_chap02.html)

> All the dependencies should be available on any Unix-like system via your package manager.

### Installation

Simply run:

```bash
$ git clone https://git.sr.ht/~voidraven/s4
$ cd s4
$ make deps
$ make install # as root

# to uninstall
$ sudo make uninstall
```

> Make sure that your user is in the *video* group

### Usage

```bash
$ s4ctl -i         # initialize the config file, watchfiles, and storage
$ s4ctl -s         # start all/some of the cameras
$ s4ctl -p         # stop all/some of the cameras
$ s4ctl -l         # list the available cameras and their status
$ s4ctl -h         # show this help information
$ s4ctl -v         # show the s4 version info
```

### Examples

> These are examples are not exact. They are not meant to be copy and paste commands, but give you the general idea of what the workflow is.

After installing we want to generate our config files for each computer. (Make sure your storage location is mounted):

```bash
$ s4ctl -i
> How many cameras do you want to setup?: 2
> What resolution are you recording at?: 1280x720
> What framerate do you want to record at? [30/60]: 30
> Where do you want to store your video?: /mnt/file-server/video
> Configuring storage...
> Generating watch files...
> OK

# this writes ~/.s4/config
```

For each computer there is generated *watch script*. Copy it wherever you want on the remote machine:

```bash
scp watch1.sh user@comp1:/home/user
```

For each of the camera computers add a cron job that will start watch.sh at boot:

```bash
(crontab -l ; echo "@reboot /home/user/watch1.sh") | crontab -
```

Make sure it installed correctly:

```bash
$ s4ctl -l

# you should see output something like this:
Camera 0 - Running Status: false
Camera 1 - Running Status: false
```

Start recording:

```bash
$ s4ctl -s
```

You should now see the status has changed to recording! 🎆

```bash
$ s4ctl -l
Camera 0 - Running Status: true
Camera 1 - Running Status: true
```

### Notes
We break up the video files every **10 minutes**. The files are named *hour-min.mkv*, for instance 

```bash
0-0.mkv   # 12:00am - 12:09 am
0-1.mkv   # 12:10am - 12:19 am
...
13-3.mkv  # 1:30pm - 1:39pm
13-4.mkv  # 1:40pm - 1:49pm
13-5.mkv  # 1:50pm - 1:59pm
14-0.mkv  # 2:00pm - 2:09pm
```

- s4 is storage agnostic. NFS is not a requirement, it's just what I use. You could use Samba, SSHFS, etc.

### TODO
- Add logging + log levels
- Pause / Resume
- Multi-camera per computer support

### License / Disclaimer
This project is licensed under the ISC license. (See LICENSE.md) Please exercise good security practice when using in a shared network environment. I'm not responsible for you setting this up poorly and your roommate Carl ends up watching you.
